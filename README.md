#Docker Certs

##Generating cert bundle
Get perl script to build CA bundle from curl repo
https://github.com/curl/curl/blob/master/lib/mk-ca-bundle.pl
```
./mk-ca-bundle.pl
mv ca-bundle.crt ca-certificates.crt
```
##Building
```
docker build .
```
